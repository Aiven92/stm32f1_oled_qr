# Вывод полученной по UART строки на oled дисплей в виде QR кода.

board: BluePill(stm32f103c8)  
oled: 128x64(SH1106)  
libs: CMSIS + HAL + FreeRTOS 9  
qr_generator: https://github.com/nayuki/QR-Code-generator  
oled: https://github.com/ananevilya/SSD1306_HAL_EN_RU с модификацией под SH1106  
compiler: arm-none-eabi-  

# Подключение:
PA9  - Tx  
PA10 - Rx  
PB10 - sck  
PB11 - sda  

# Структура:
 repo  
 |-submodules  
 | |-CMSIS  
 | |-FreeRTOS  
 | |-QR-Code-generator  
 | |-SSD1306_HAL_EN_RU  
 | \-STM32F1xx_HAL_Driver  
 \\-f1_project  
    |-Inc  
    |-Src  
    |-Makefile  
    |-startup_stm32f103xb.s  
    \\-STM32F103C8Tx_FLASH.ld  
   
# Сборка:
git clone https://bitbucket.org/Aiven92/stm32f1_oled_qr  
cd stm32f1_oled_qr  
git submodule update --init --recursive  
cd f1_project  
make  
make flash  