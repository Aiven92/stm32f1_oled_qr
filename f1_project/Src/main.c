/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f1xx_hal.h"
#include "cmsis_os.h"
// #include "FreeRTOS.h"
// #include "task.h"
// #include "queue.h"

#include "qrcodegen.h"
#include "ssd1306.h"

#define DBGPRINT

I2C_HandleTypeDef hi2c2;
UART_HandleTypeDef huart1;

#define DataLength 20
uint8_t RxBuffer[DataLength];
uint8_t StartPositionBuffer=0;
uint8_t EndPositionBuffer=0;
volatile uint8_t rec;

// QR_generator buffers
#define qrcodegen_BUFFER_LEN_MAX 100
uint8_t qr0[qrcodegen_BUFFER_LEN_MAX];
uint8_t tempBuffer[qrcodegen_BUFFER_LEN_MAX];

// command MUST BE associate to CommandType
uint8_t commands[][3] = { "dqr","cqr","dst","cst"};
enum CommandType{
	UNNOWN = -1,
    DISPLAY_QR,     //qr on display
    CONSOLE_QR,     //qr in console
    DISPLAY_STRING, //string on display
    CONSOLE_STRING, //string in console
};

struct MessageStruct{
    enum CommandType Type;
    uint8_t DataString[DataLength];
};

/* USER CODE END PV */

void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_I2C2_Init(void);
static void MX_USART1_UART_Init(void);


QueueHandle_t ParsedMessageCONSOLEQueue;
QueueHandle_t ParsedMessageDISPLAYQueue;
QueueHandle_t RawMessageQueue;
QueueHandle_t DisplayQueue;


void BlinkTask(void const * argument);
void UARTComandDecoder(void const * argument);
void DisplayTask(void const * argument);
void ConsoleTask(void const * argument);

// uart rx handler
void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart){
    
    if(huart == &huart1){
        
        HAL_UART_Receive_IT(&huart1, &rec, 1);
        
        //send message by EOL
        if(rec == '\n' || rec == '\r'){
            xQueueSendFromISR( RawMessageQueue, &RxBuffer, NULL );
            EndPositionBuffer=0;
            //erase rx buffer
            memset(&RxBuffer,0,DataLength);
        }else{
            RxBuffer[EndPositionBuffer++]=rec;
        }
    }
}

int main(void){

    HAL_Init();
    SystemClock_Config();
    MX_GPIO_Init();
    MX_I2C2_Init();
    MX_USART1_UART_Init();
    
    ssd1306_Init(hi2c2, 0x78);
    ssd1306_SetContrast(255);
    ssd1306_Fill(White);
    
    
    HAL_UART_Receive_IT(&huart1, &rec, 1);


    xTaskCreate(    BlinkTask,
                    "BlinkTask",
                    128,
                    ( void * ) 1,
                    1,
                    NULL);
    
    xTaskCreate(    UARTComandDecoder,
                    "ComandDecoder",
                    128,
                    ( void * ) 1,
                    2,
                    NULL);
    
    xTaskCreate(    DisplayTask,
                    "DisplayTsk",
                    128,
                    ( void * ) 1,
                    2,
                    NULL);
    xTaskCreate(    ConsoleTask,
                    "ConsoleTask",
                    128,
                    ( void * ) 1,
                    2,
                    NULL);
    
    RawMessageQueue             = xQueueCreate( 1, sizeof( RxBuffer ));
    ParsedMessageDISPLAYQueue   = xQueueCreate( 1, sizeof( struct MessageStruct ));
    ParsedMessageCONSOLEQueue   = xQueueCreate( 1, sizeof( struct MessageStruct ));
    
//     vTaskStartScheduler();
     osKernelStart();

    while (1){
    }

}

/* StartDefaultTask function */
void BlinkTask(void const * argument){
    uint32_t i=0;
    for(;;){
        //just a blink 
        HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_SET);
        vTaskDelay( 300 );
        HAL_GPIO_WritePin(GPIOC,GPIO_PIN_13,GPIO_PIN_RESET);
        #ifdef DBGPRINT
        printf("DefaultTask: %d\r\n",i++);
        #endif
        vTaskDelay( 300 );
  }

}

void UARTComandDecoder(void const * argument){

    uint8_t RawString[40];
    struct MessageStruct Message;ssd1306_UpdateScreen();
    for(;;){
        xQueueReceive( RawMessageQueue, &( RawString ), portMAX_DELAY);
        
        #ifdef DBGPRINT
        printf("UARTComandDecoder: recieved raw string = %s\r\n", RawString);
        printf("UARTComandDecoder: compare = %d\r\n", strncmp(RawString, commands[0], 3));
        #endif
        
        //compare first 3 character of raw string for command detect
        Message.Type = UNNOWN;
        if(strncmp(RawString, commands[DISPLAY_QR], 3) == 0)
            Message.Type = DISPLAY_QR;
        else if(strncmp(RawString, commands[CONSOLE_QR], 3) == 0)
            Message.Type = CONSOLE_QR;
        else if(strncmp(RawString, commands[DISPLAY_STRING], 3) == 0)
            Message.Type = DISPLAY_STRING;
        else if(strncmp(RawString, commands[CONSOLE_STRING], 3) == 0)
            Message.Type = CONSOLE_STRING;
        
        #ifdef DBGPRINT
        printf("UARTComandDecoder: Message.Type = %d\r\n", Message.Type);
        #endif
        
        //send message to right queue
        strcpy (Message.DataString, &RawString[4]);
        switch( Message.Type ){
            case DISPLAY_QR:
                xQueueSend( ParsedMessageDISPLAYQueue, ( void * ) &Message, portMAX_DELAY );
                break;
            case DISPLAY_STRING:
                xQueueSend( ParsedMessageDISPLAYQueue, ( void * ) &Message, portMAX_DELAY );
                break;
            case CONSOLE_QR:
                xQueueSend( ParsedMessageCONSOLEQueue, ( void * ) &Message, portMAX_DELAY );
                break;
            case CONSOLE_STRING:
                xQueueSend( ParsedMessageCONSOLEQueue, ( void * ) &Message, portMAX_DELAY );
                break;
            case UNNOWN:
                break;
            default:
            	break;
        }
               
    }
}

void DisplayTask(void const * argument){
    
    struct MessageStruct Message;
    uint8_t stringNumber=0;
    int scale =3;
    for(;;){
        xQueueReceive( ParsedMessageDISPLAYQueue, &( Message ), portMAX_DELAY);
        
        #ifdef DBGPRINT
        printf("DisplayTask: recieved command = %d\r\n", Message.Type);
        printf("DisplayTask: recieved string = %s\r\n", Message.DataString);
        #endif
        
        
        if(Message.Type == DISPLAY_QR){
            ssd1306_Fill(White);
            //generate qr mask
            bool ok = qrcodegen_encodeText( Message.DataString,
                                tempBuffer, qr0, qrcodegen_Ecc_MEDIUM,
                                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX,
                                qrcodegen_Mask_AUTO, true);
            #ifdef DBGPRINT
            if(ok) 
                printf("DisplayTask: qr_generator - ok\r\n");
            else 
                printf("DisplayTask: qr_generator - error\r\n");
            #endif
            
            int size = qrcodegen_getSize(qr0);
            
            #ifdef DBGPRINT
            printf("DisplayTask: size = %d\r\n",size);
            #endif
            
            //fill dispaly buffer up with qr mask with additional scale factor 
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    bool pixel = qrcodegen_getModule(qr0, x, y);
                    for(int i=1;i<=scale;i++){
                        for(int j=1;j<=scale;j++){
                            ssd1306_DrawPixel(x*scale+i,y*scale+j,!pixel);
                        }
                    }
                }
            }
            ssd1306_UpdateScreen();
            stringNumber = 0;
        }
        if(Message.Type == DISPLAY_STRING){
            char buffer [20];
            
            if(stringNumber == 0)
                ssd1306_Fill(White);
            ssd1306_SetCursor(0,stringNumber*9);
            ssd1306_WriteString(Message.DataString, Font_7x9, White);
            stringNumber++;
            if(stringNumber>=7) stringNumber=0;
            ssd1306_UpdateScreen();
        }   
    }

}

void ConsoleTask(void const * argument){
    
    struct MessageStruct Message;
    for(;;){
        xQueueReceive( ParsedMessageCONSOLEQueue, &( Message ), portMAX_DELAY);
        
        #ifdef DBGPRINT
        printf("ConsoleTask: recieved command = %d\r\n", Message.Type);
        printf("ConsoleTask: recieved string = %s\r\n", Message.DataString);
        #endif
        
        if(Message.Type == CONSOLE_QR){
            bool ok = qrcodegen_encodeText( Message.DataString,
                                tempBuffer, qr0, qrcodegen_Ecc_MEDIUM,
                                qrcodegen_VERSION_MIN, qrcodegen_VERSION_MAX,
                                qrcodegen_Mask_AUTO, true);
            #ifdef DBGPRINT
            if(ok) 
                printf("ConsoleTask: qr_generator - ok\r\n");
            else 
                printf("ConsoleTask: qr_generator - error\r\n");
            #endif
            
            int size = qrcodegen_getSize(qr0);
            
            #ifdef DBGPRINT
            printf("ConsoleTask: size = %d\r\n",size);
            #endif
            
            for (int y = 0; y < size; y++) {
                for (int x = 0; x < size; x++) {
                    bool pixel = qrcodegen_getModule(qr0, x, y);
                    
                    if(pixel)
                        HAL_UART_Transmit(&huart1," ",1,0xFFFF);
                    else
                        HAL_UART_Transmit(&huart1,"x",1,0xFFFF);
                }
                HAL_UART_Transmit(&huart1,"\r\n",2,0xFFFF);
            }
        }
        
        if(Message.Type == CONSOLE_STRING){
            printf("ConsoleTask: recieved string = %s\r\n", Message.DataString);
        }   
    }
}



void SystemClock_Config(void){

    RCC_OscInitTypeDef RCC_OscInitStruct;
    RCC_ClkInitTypeDef RCC_ClkInitStruct;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
    RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE;
    RCC_OscInitStruct.HSEState = RCC_HSE_ON;
    RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
    RCC_OscInitStruct.HSIState = RCC_HSI_ON;
    RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
    RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
    RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
    if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
    {
    _Error_Handler(__FILE__, __LINE__);
    }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
    RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                                |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
    RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
    RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
    RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
    RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

    if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
    {
    _Error_Handler(__FILE__, __LINE__);
    }

    /**Configure the Systick interrupt time 
    */
    HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
    HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

    /* SysTick_IRQn interrupt configuration */
    HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* I2C2 init function */
static void MX_I2C2_Init(void){

  hi2c2.Instance = I2C2;
  hi2c2.Init.ClockSpeed = 400000;
  hi2c2.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c2.Init.OwnAddress1 = 0;
  hi2c2.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c2.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c2.Init.OwnAddress2 = 0;
  hi2c2.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c2.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  
  if (HAL_I2C_Init(&hi2c2) != HAL_OK){
    _Error_Handler(__FILE__, __LINE__);
  }

}

/* USART1 init function */
static void MX_USART1_UART_Init(void){

    huart1.Instance = USART1;
    huart1.Init.BaudRate = 115200;
    huart1.Init.WordLength = UART_WORDLENGTH_8B;
    huart1.Init.StopBits = UART_STOPBITS_1;
    huart1.Init.Parity = UART_PARITY_NONE;
    huart1.Init.Mode = UART_MODE_TX_RX;
    huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
    huart1.Init.OverSampling = UART_OVERSAMPLING_16;
    if (HAL_UART_Init(&huart1) != HAL_OK){
        _Error_Handler(__FILE__, __LINE__);
    }

}

/** Configure pins as 
        * Analog 
        * Input 
        * Output
        * EVENT_OUT
        * EXTI
*/
static void MX_GPIO_Init(void){

    GPIO_InitTypeDef GPIO_InitStruct;

    /* GPIO Ports Clock Enable */
    __HAL_RCC_GPIOC_CLK_ENABLE();
    __HAL_RCC_GPIOD_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
    __HAL_RCC_GPIOA_CLK_ENABLE();

    /*Configure GPIO pin Output Level */
    HAL_GPIO_WritePin(GPIOC, GPIO_PIN_13, GPIO_PIN_RESET);

    /*Configure GPIO pin : PC13 */
    GPIO_InitStruct.Pin = GPIO_PIN_13;
    GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
    GPIO_InitStruct.Pull = GPIO_NOPULL;
    GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
    HAL_GPIO_Init(GPIOC, &GPIO_InitStruct);

}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim){
    
    if (htim->Instance == TIM1) {
        HAL_IncTick();
    }
}

void _Error_Handler(char *file, int line){
    
  while(1){
  }
}

#ifdef __GNUC__
/* With GCC/RAISONANCE, small printf (option LD Linker->Libraries->Small printf
   set to 'Yes') calls __io_putchar() */
#define PUTCHAR_PROTOTYPE int __io_putchar(int ch)
#else
#define PUTCHAR_PROTOTYPE int fputc(int ch, FILE *f)
#endif /* __GNUC__ */
PUTCHAR_PROTOTYPE
{
  /* Place your implementation of fputc here */
  /* e.g. write a character to the USART1 and Loop until the end of transmission */
  HAL_UART_Transmit(&huart1, (uint8_t *)&ch, 1, 0xFFFF);

  return ch;
}
int _write(int file, char *ptr, int len)
{
	int DataIdx;

	for (DataIdx = 0; DataIdx < len; DataIdx++)
	{
		__io_putchar(*ptr++);
	}
	return len;
}

#ifdef  USE_FULL_ASSERT
// void assert_failed(uint8_t* file, uint32_t line){ 
//   /* USER CODE BEGIN 6 */
//   /* User can add his own implementation to report the file name and line number,
//      tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
//   /* USER CODE END 6 */
// }
#endif /* USE_FULL_ASSERT */
